package com.chang.nov.mygdxgame.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.chang.nov.mygdxgame.model.Block;
import com.chang.nov.mygdxgame.model.Bob;
import com.chang.nov.mygdxgame.model.Bob.State;
import com.chang.nov.mygdxgame.model.Enemy;
import com.chang.nov.mygdxgame.model.EnemyLaser;
import com.chang.nov.mygdxgame.model.Laser;
import com.chang.nov.mygdxgame.model.World;

public class WorldRenderer {

	private static final float CAMERA_WIDTH = 10f;
	private static final float CAMERA_HEIGHT = 7f;
	private static final float RUNNING_FRAME_DURATION = 0.06f;

	private TextureRegion bobIdleLeft;
	private TextureRegion bobIdleRight;
	private TextureRegion blockTexture;
	private TextureRegion bobFrame;
	private TextureRegion bobJumpLeft;
	private TextureRegion bobJumpRight;
	private TextureRegion bobFallLeft;
	private TextureRegion bobFallRight;
	private TextureRegion laserTexture = new TextureRegion();
	private TextureRegion enemyTexture;

	private Animation walkLeftAnimation;
	private Animation walkRightAnimation;

	private World world;
	private OrthographicCamera cam;

	ShapeRenderer debugRenderer = new ShapeRenderer();

	// private Texture bobTexture;
	// private Texture blockTexture;

	private SpriteBatch spriteBatch;
	private boolean debug = false;
	private boolean drawCollisionBlocks = false;
	private int width;
	private int height;
	private float ppuX;
	private float ppuY;

	public void setSize(int w, int h) {
		this.width = w;
		this.height = h;
		ppuX = (float) width / CAMERA_WIDTH;
		ppuY = (float) height / CAMERA_HEIGHT;
	}

	public WorldRenderer(World world) {
		this.world = world;
		this.cam = new OrthographicCamera(10, 7);
		this.cam.position.set(5, 3.5f, 0);
		this.cam.update();
		spriteBatch = new SpriteBatch();
		loadTextures();

	}

	private void loadTextures() {
		// bobTexture = new Texture(Gdx.files.internal("data/bob_01.png"));
		// blockTexture = new Texture(Gdx.files.internal("data/block.png"));
		TextureAtlas atlas = new TextureAtlas(
				Gdx.files.internal("data/textures/textures.pack"));
		bobIdleLeft = atlas.findRegion("bob-01");
		bobIdleRight = new TextureRegion(bobIdleLeft);
		bobIdleRight.flip(true, false);
		blockTexture = atlas.findRegion("block");
		laserTexture = atlas.findRegion("laser");
		enemyTexture = atlas.findRegion("enemy");
		bobJumpLeft = atlas.findRegion("bob-up");
		bobJumpRight = new TextureRegion(bobJumpLeft);
		bobJumpRight.flip(true, false);
		bobFallLeft = atlas.findRegion("bob-down");
		bobFallRight = new TextureRegion(bobFallLeft);
		bobFallRight.flip(true, false);

		TextureRegion[] walkLeftFrames = new TextureRegion[5];
		for (int i = 0; i < 5; i++) {
			walkLeftFrames[i] = atlas.findRegion("bob-0" + (i + 2));
		}
		walkLeftAnimation = new Animation(RUNNING_FRAME_DURATION,
				walkLeftFrames);

		TextureRegion[] walkRightFrames = new TextureRegion[5];
		for (int i = 0; i < 5; i++) {
			walkRightFrames[i] = new TextureRegion(walkLeftFrames[i]);
			walkRightFrames[i].flip(true, false);
		}

		walkRightAnimation = new Animation(RUNNING_FRAME_DURATION,
				walkRightFrames);

	}

	public void render() {
		spriteBatch.begin();
		drawBlocks();
		drawBob();
		drawLasers();
		drawEnemies();
		spriteBatch.end();
		if (debug)
			drawDebug();
		if (drawCollisionBlocks)
			drawCollisionBlocks();
			
	}

	private void drawEnemies() {
		for (Enemy enemy : world.getEnemies()) {
			spriteBatch.draw(enemyTexture, enemy.getPosition().x * ppuX, enemy.getPosition().y * ppuY, enemy.getBounds().width * ppuX, enemy.getBounds().height * ppuY);
		}
		
	}

	private void drawCollisionBlocks() {
		debugRenderer.setProjectionMatrix(cam.combined);
		debugRenderer.begin(ShapeType.Filled);
		debugRenderer.setColor(new Color(1, 1, 1, 1));
		for (Rectangle rect : world.getCollisionRects()) {
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		debugRenderer.end();
	}

	public void toggleDebug() {
		debug = !debug;
	}

	private void drawBlocks() {
		for (Block block : world.getDrawableBlocks((int)CAMERA_WIDTH, (int)CAMERA_HEIGHT)) {
			spriteBatch.draw(blockTexture, block.getPosition().x * ppuX,
					block.getPosition().y * ppuY, Block.SIZE * ppuX, Block.SIZE
							* ppuY);
		}
	}
	private void drawLasers() {
		if (world.getLasers().size > 0) {
			for (Laser laser : world.getLasers()) {
				spriteBatch.draw(laserTexture, laser.getPosition().x * ppuX, laser.getPosition().y * ppuY, laser.getBounds().width * ppuX, laser.getBounds().height * ppuY);							
			}
		}
		if (world.getEnemyLasers().size > 0) {
			for (EnemyLaser laser : world.getEnemyLasers()) {
				spriteBatch.draw(laserTexture, laser.getPosition().x * ppuX, laser.getPosition().y * ppuY, laser.getBounds().width * ppuX, laser.getBounds().height * ppuY);
			}
		}
		
	}

	private void drawBob() {
		Bob bob = world.getBob();
		bobFrame = bob.isFacingLeft() ? bobIdleLeft : bobIdleRight;
		if (bob.getState().equals(State.WALKING)) {
			bobFrame = bob.isFacingLeft() ? walkLeftAnimation.getKeyFrame(
					bob.getStateTime(), true) : walkRightAnimation.getKeyFrame(
					bob.getStateTime(), true);
		} else if (bob.getState().equals(State.JUMPING)) {
			if (bob.getVelocity().y > 0) {
				bobFrame = bob.isFacingLeft() ? bobJumpLeft : bobJumpRight;
			} else {
				bobFrame = bob.isFacingLeft() ? bobFallLeft : bobFallRight;
			}
		}
		spriteBatch.draw(bobFrame, bob.getPosition().x * ppuX,
				bob.getPosition().y * ppuY, Bob.SIZE * ppuX, Bob.SIZE * ppuY);
	}

	private void drawDebug() {
		debugRenderer.setProjectionMatrix(cam.combined);
		debugRenderer.begin(ShapeType.Line);
		for (Block block : world.getDrawableBlocks((int)CAMERA_WIDTH, (int)CAMERA_HEIGHT)) {
			Rectangle rect = block.getBounds();
			debugRenderer.setColor(new Color(1, 0, 0, 1));
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);

		}
		for (Laser lase : world.getLasers()) {
			Rectangle rect = lase.getBounds();
			debugRenderer.setColor(new Color(1, 1, 1, 1));
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		for (EnemyLaser laser : world.getEnemyLasers()) {
			Rectangle rect = laser.getBounds();
			debugRenderer.setColor(new Color(1, 1, 1, 1));
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);

		}
		for (Enemy enemy : world.getEnemies()) {
			Rectangle rect = enemy.getBounds();
			debugRenderer.setColor(new Color(0, 0, 1, 1));
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}

		Bob bob = world.getBob();
		Rectangle rect = bob.getBounds();
		debugRenderer.setColor(new Color(0, 1, 0, 1));
		debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		debugRenderer.end();
	}

	public void toggleCollisionBlocks() {
		drawCollisionBlocks = !drawCollisionBlocks;
	}
}
