package com.chang.nov.mygdxgame.screens;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.chang.nov.mygdxgame.controller.BobController;
import com.chang.nov.mygdxgame.controller.EnemyController;
import com.chang.nov.mygdxgame.model.World;
import com.chang.nov.mygdxgame.view.WorldRenderer;

public class GameScreen implements Screen, InputProcessor {
	
	private World world;
	private WorldRenderer renderer;
	private BobController controller;
	private EnemyController eController;
	private int width, height;
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		controller.update(delta);
		eController.update(delta);
		renderer.render();
	}

	@Override
	public void resize(int width, int height) {
		renderer.setSize(width, height);
		this.width = width;
		this.height = height;
	}

	@Override
	public void show() {
		world = new World();
		renderer = new WorldRenderer(world);
		controller = new BobController(world);
		eController = new EnemyController(world);
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.LEFT)
			controller.leftPressed();
		if (keycode == Keys.RIGHT)
			controller.rightPressed();
		if (keycode == Keys.Z || keycode == Keys.UP)
			controller.jumpPressed();
		if (keycode == Keys.X || keycode == Keys.SPACE)
			controller.firePressed();
		if (keycode == Keys.D)
			renderer.toggleDebug();
		if (keycode == Keys.C)
			renderer.toggleCollisionBlocks();
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.LEFT)
			controller.leftReleased();
		if (keycode == Keys.RIGHT)
			controller.rightReleased();
		if (keycode == Keys.Z || keycode == Keys.UP)
			controller.jumpReleased();
		if (keycode == Keys.X || keycode == Keys.SPACE)
			controller.fireReleased();
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (!Gdx.app.getType().equals(ApplicationType.Android))
			return false;
		if (screenX < width / 4 & screenY < height / 4) {
			renderer.toggleDebug();
		}
		else if (screenX < width / 4 && (screenY > height / 4 && screenY < height /2 )) {
			renderer.toggleCollisionBlocks();
		}
		else {
			if (screenX < width / 8 && screenY > height / 2) {
				controller.leftPressed();
			}
			if (screenX < width / 2 && screenX > width / 8 && screenY > height / 2) {
				controller.rightPressed();
			}
			if (screenX > (3 * width / 4) && screenY > height / 2) {
				controller.jumpPressed();
			}
			if (screenX > width / 2 && screenX < (3 * width / 4) && screenY > height / 2) {
				controller.firePressed();
			}
		}
		
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (!Gdx.app.getType().equals(ApplicationType.Android))
			return false;
		if (screenX < width / 4 && screenY > height / 2) {
			controller.leftReleased();
			controller.rightReleased();
			//controller.jumpReleased();
			//controller.fireReleased();
		}
		if (screenX < width / 2 && screenX > width /4 && screenY > height / 2) {
			controller.rightReleased();
			controller.leftReleased();
			//controller.jumpReleased();
			//controller.fireReleased();
		}
		if (screenX > (3 * width / 4) && screenY > height / 2) {
			controller.jumpReleased();
			controller.fireReleased();
			//controller.leftReleased();
			//controller.rightReleased();
		}
		if (screenX > width / 2 && screenX < (3 * width / 4) && screenY > height / 2) {
			controller.fireReleased();
			controller.jumpReleased();
			//controller.leftReleased();
			//controller.rightReleased();
			
		}
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
