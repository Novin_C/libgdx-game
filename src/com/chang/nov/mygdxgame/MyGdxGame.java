package com.chang.nov.mygdxgame;

import com.badlogic.gdx.Game;
import com.chang.nov.mygdxgame.screens.GameScreen;

public class MyGdxGame extends Game {
	
	@Override
	public void create() {		
		setScreen(new GameScreen());
	}
	
	@Override
	public void resize(int width, int height) {
		//Do nothing
	}
}
