package com.chang.nov.mygdxgame.model;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class World {
	
	//Array<Block> blocks = new Array<Block>();
	Bob bob;
	Level level;
	Array<Rectangle> collisionRects = new Array<Rectangle>();
	Array<Laser> lasers = new Array<Laser>();
	Array<Enemy> enemies = new Array<Enemy>();
	private Array<EnemyLaser> enemyLasers = new Array<EnemyLaser>();
	
	public Array<Rectangle> getCollisionRects() {
		return collisionRects;
	}
	
//	public Array<Block> getBlocks() {
//		return blocks;
//	}
	
	public Bob getBob() {
		return bob;
	}
	
	public Level getLevel() {
		return level;
	}
	
	public World() {
		createDemoWorld();
		
	}
	
	public List<Block> getDrawableBlocks(int width, int height) {
		int x = (int)bob.getPosition().x - width;
		int y = (int)bob.getPosition().y - height;
		if (x < 0) {
			x = 0;
		}
		if (y < 0) {
			y = 0;
		}
		int x2 = x + 2 * width;
		int y2 = y + 2 * height;
		if (x2 > level.getWidth()) {
			x2 = level.getWidth() - 1;
		}
		if (y2 > level.getHeight()) {
			y2 = level.getHeight() - 1;
		}
		
		List<Block> blocks = new ArrayList<Block>();
		Block block;
		for (int col = x; col <= x2; col++) {
			for (int row = y; row <= y2; row++) {
				block = level.getBlocks()[col][row];
				if (block != null) {
					blocks.add(block);
				}
			}
		}
		return blocks;
	}

	private void createDemoWorld() {
		bob = new Bob(new Vector2(7, (float) 3));
		enemies.add(new Enemy(new Vector2(1.5f, 4f)));
		//enemies.add(new Enemy(new Vector2(2.5f, 4f)));
		//enemies.add(new Enemy(new Vector2(0.5f, 4f)));
		level = new Level();
	}

	public Array<Laser> getLasers() {
		return lasers;
	}
	
	public Array<Enemy> getEnemies() {
		return enemies;
	}

	public Array<EnemyLaser> getEnemyLasers() {
		// TODO Auto-generated method stub
		return enemyLasers;
	}
}
