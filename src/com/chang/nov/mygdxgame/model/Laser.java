package com.chang.nov.mygdxgame.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Laser {
	
	private final float SPEED = 25f;
	private final static float SHOOTSPEED = 200f;
	private Vector2 position = new Vector2();
	private Vector2 velocity = new Vector2();
	private Rectangle bounds = new Rectangle();
	private final float WIDTH = 1f;
	private final float HEIGHT = (float) (1.0 / 17);
	
	public Laser(float x, float y, Vector2 velocity) {
		this.position = new Vector2(x, (float) (y + 0.3));
		this.velocity = velocity.scl(SPEED);
		this.bounds.x = position.x;
		this.bounds.y = position.y;
		this.bounds.width = WIDTH;
		this.bounds.height = HEIGHT;
	}
	
	public void update(float delta) {
		position.add(velocity.cpy().scl(delta));
		bounds.x = position.x;
		bounds.y = position.y; 
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}

	public float getSPEED() {
		return SPEED;
	}

	public static float getTHRESHOLD() {
		return SHOOTSPEED;
	}

	public float getWIDTH() {
		return WIDTH;
	}

	public float getHEIGHT() {
		return HEIGHT;
	}
}
