package com.chang.nov.mygdxgame.model;

import com.badlogic.gdx.math.Vector2;

public class EnemyLaser extends Laser{

	public EnemyLaser(float x, float y, Vector2 velocity) {
		super(x, y, velocity);
	}
	
	public void update(float delta) {
		super.update(delta);
	}
	
}
