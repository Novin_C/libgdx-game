package com.chang.nov.mygdxgame.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Bob {
	
	public enum State {
		IDLE, WALKING, JUMPING, DYING
	}
	
	public static final float SPEED = 4f;
	static final float JUMP_VELOCITY = 1f;
	public static final float SIZE = 0.5f;
	
	Vector2 position = new Vector2();
	Vector2 acceleration = new Vector2();
	Vector2 velocity = new Vector2();
	Rectangle bounds = new Rectangle();
	State state = State.IDLE;
	boolean facingLeft = true;
	float stateTime = 0;
	boolean longJump;	
	
	public Bob(Vector2 position) {
		this.position = position;
		this.bounds.height = SIZE;
		this.bounds.width = SIZE;
	}
	
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	
	public Vector2 getPosition() {
		return position;
	}

	public void update(float delta) {
//		position.add(velocity.cpy().scl(delta));
//		bounds.x = position.x;
//		bounds.y = position.y;
		stateTime += delta;
	}

	public void setFacingLeft(boolean b) {
		facingLeft = b;
	}

	public void setState(State st) {
		this.state = st;
	}
	
	public void setAcceleration(Vector2 acceleration) {
		this.acceleration = acceleration;
	}
	
	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public Vector2 getAcceleration() {
		return acceleration;
	}
	
	public boolean isLongJump() {
		return longJump;
	}
	
	public void setLongJump(boolean longJump) {
		this.longJump = longJump;
	}


	public State getState() {
		return state;
	}


	public boolean isFacingLeft() {
		return facingLeft;
	}


	public float getStateTime() {
		return stateTime;
	}

	public void setStateTime(float stateTime) {
		this.stateTime = stateTime;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
		this.bounds.setX(position.x);
		this.bounds.setY(position.y);
	}
}
