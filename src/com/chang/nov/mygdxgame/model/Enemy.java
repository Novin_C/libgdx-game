package com.chang.nov.mygdxgame.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Enemy {
	
	private final float WIDTH = 0.5f;
	private final float HEIGHT = 0.9f;
	private float shootTime;
	Vector2 position = new Vector2();
	Rectangle bounds = new Rectangle();
	
	public Enemy(Vector2 position) {
		this.position = position;
		this.bounds.x = position.x;
		this.bounds.y = position.y;
		this.bounds.width = WIDTH;
		this.bounds.height = HEIGHT;
		shootTime = (float) (100);
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	
	public void update(World world, float delta) {
		for (EnemyLaser laser : world.getEnemyLasers()) {
			laser.update(delta);
		}
	}

	public float getShootTime() {
		return shootTime;
	}

	public void setShootTime(float shootTime) {
		this.shootTime = shootTime;
	}
	
	
	
}
