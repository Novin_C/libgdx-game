package com.chang.nov.mygdxgame.controller;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.chang.nov.mygdxgame.model.Enemy;
import com.chang.nov.mygdxgame.model.EnemyLaser;
import com.chang.nov.mygdxgame.model.World;

public class EnemyController {
	
	World world;
	Array<Enemy> enemies;
	private float time;

	public EnemyController(World world) {
		this.world = world;
		this.enemies = world.getEnemies();
		//this.time = 0;
	}

	public void update(float delta) {
//		if (time == 0) {
//			time = System.currentTimeMillis();
//		}
		//time = System.currentTimeMillis();

		for (Enemy enemy : enemies) {
			//time = System.currentTimeMillis();

			if ((System.currentTimeMillis() - time) >= enemy.getShootTime()) {
				//time = System.currentTimeMillis();
				world.getEnemyLasers().add(new EnemyLaser(enemy.getBounds().x, enemy.getBounds().y + 0.3f, new Vector2(1, 0)));
				time = System.currentTimeMillis();

			}
			//time = System.currentTimeMillis();

			enemy.update(world, delta);
		}
//		for (EnemyLaser laser : world.getEnemyLasers()) {
//			laser.update(delta);			
//		}
		
	}

}
