package com.chang.nov.mygdxgame.controller;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.chang.nov.mygdxgame.model.Block;
import com.chang.nov.mygdxgame.model.Bob;
import com.chang.nov.mygdxgame.model.Bob.State;
import com.chang.nov.mygdxgame.model.Enemy;
import com.chang.nov.mygdxgame.model.Laser;
import com.chang.nov.mygdxgame.model.World;

public class BobController {

	enum Keys {
		LEFT, RIGHT, JUMP, FIRE
	}
	
	private Array<Block> collidable = new Array<Block>();
	
	private static final long LONG_JUMP_PRESS = 250;
	private static final float ACCELERATION = 20f;
	private static final float GRAVITY = -20f;
	private static final float MAX_JUMP_SPEED = 7f;
	private static final float DAMP = 0.90f;
	private static final float MAX_VEL = 4f;
	
	@SuppressWarnings("unused")
	private static final float WIDTH = 10f;
	
	private World 	world;
	private Bob 	bob;
	private Array<Laser> lasers;
	private long jumpPressedTime;
	private boolean jumpingPressed;
	private boolean firePressed;
	private long firePressedTime;
	private boolean grounded = false;
	private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject() {
			return new Rectangle();
		}
		
	};
	
	private Pool<Rectangle> rectPool2 = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject() {
			return new Rectangle();
		}
		
	};
	
	static Map<Keys, Boolean> keys = new HashMap<BobController.Keys, Boolean>();
	static {
		keys.put(Keys.LEFT, false);
		keys.put(Keys.RIGHT, false);
		keys.put(Keys.JUMP, false);
		keys.put(Keys.FIRE, false);
	};

	public BobController(World world) {
		this.world = world;
		this.bob = world.getBob();
		this.lasers = world.getLasers();
	}

	// ** Key presses and touches **************** //
	
	public void leftPressed() {
		keys.get(keys.put(Keys.LEFT, true));
	}
	
	public void rightPressed() {
		keys.get(keys.put(Keys.RIGHT, true));
	}
	
	public void jumpPressed() {
		keys.get(keys.put(Keys.JUMP, true));
		// jumpingPressed = true;
	}
	
	public void firePressed() {
		keys.get(keys.put(Keys.FIRE, true));
		firePressed = true;
	}
	
	public void leftReleased() {
		keys.get(keys.put(Keys.LEFT, false));
	}
	
	public void rightReleased() {
		keys.get(keys.put(Keys.RIGHT, false));
	}
	
	public void jumpReleased() {
		keys.get(keys.put(Keys.JUMP, false));
		jumpingPressed = false;
	}
	
	public void fireReleased() {
		keys.get(keys.put(Keys.FIRE, false));
		firePressed = false;
	}
	
	/** The main update method **/
	public void update(float delta) {
		processInput();
		processFire();
		if (grounded && bob.getState().equals(State.JUMPING)) {
			bob.setState(State.IDLE);
		}
		
		bob.getAcceleration().y = GRAVITY;
		bob.getAcceleration().scl(delta);
		bob.getVelocity().add(bob.getAcceleration().x, bob.getAcceleration().y);
		checkCollisionWithBlocks(delta);
		
		bob.getVelocity().x *= DAMP;
		if (bob.getAcceleration().x == 0) bob.getVelocity().x *= DAMP;
		if (bob.getVelocity().x > MAX_VEL) {
			bob.getVelocity().x = MAX_VEL;
		}
		if (bob.getVelocity().x < -MAX_VEL) {
			bob.getVelocity().x = -MAX_VEL;
		}
		
//		if (bob.getPosition().y < 0) {
//			bob.getPosition().y = 0f;
//			bob.setPosition(bob.getPosition());
//			if (bob.getState().equals(State.JUMPING)) {
//				bob.setState(State.IDLE);
//			}
//		}
		if (bob.getPosition().x < 0) {
			//bob.getPosition().y = 1;
			bob.getPosition().x = 0;
			bob.getVelocity().x = 0;
			bob.setPosition(bob.getPosition());
//			bob.getPosition().x = 0;
//			bob.setPosition(bob.getPosition());
//			if (!bob.getState().equals(State.JUMPING)) {
//				bob.setState(State.IDLE);
//			}
		}
		if (bob.getPosition().y < 0) {
			bob.getPosition().x = 7;
			bob.getPosition().y = 3;
			bob.getVelocity().y = 0;
			bob.setPosition(bob.getPosition());
			bob.setState(State.IDLE);
		}
		for (Laser laser : world.getLasers()) {
			if (laser.getPosition().x < (0 - laser.getWIDTH()) || laser.getPosition().x > 10) {
				world.getLasers().removeValue(laser, false);
				//System.out.println("deleted");
			}
		}
//		if (bob.getPosition().x > WIDTH - bob.getBounds().width) {
//			bob.getPosition().x = WIDTH - bob.getBounds().width;
//			bob.setPosition(bob.getPosition());
//			if (!bob.getState().equals(State.JUMPING)) {
//				bob.setState(State.IDLE);
//			}
//		}
		bob.update(delta);
		checkLaserCollision(delta);
		for (Laser l : lasers) {
			l.update(delta);
		}
	}
	
	private void processFire() {
		if (keys.get(Keys.FIRE)) {
//			if (world.getLasers().size == 0) {
//				firePressed = true;
//				firePressedTime = System.currentTimeMillis();
//				float temp = 1;
//				if (world.getBob().isFacingLeft()) temp = -1;
//				world.getLasers().add(new Laser(bob.getPosition().x, bob.getPosition().y, new Vector2(temp, 0)));
				//System.out.println("fire"  );
			//}
			firePressed = true;
				if (firePressed && ((System.currentTimeMillis() - firePressedTime) >= Laser.getTHRESHOLD())) { //reaches over threshold
					// create another
					//System.out.println("create");
					firePressedTime = System.currentTimeMillis();
					float temp = 1;
					float posX = bob.getPosition().x;
					if (!world.getBob().isFacingLeft()) posX += Bob.SIZE - 0.5f;
					if (world.getBob().isFacingLeft()) {
						temp = -1;
						Laser tempL = new Laser(0, 0, new Vector2(0, 0));
						posX -= tempL.getWIDTH() - 0.5f;
					}
					world.getLasers().add(new Laser(posX, bob.getPosition().y, new Vector2(temp, 0)));
					firePressedTime = System.currentTimeMillis();
					
				} else { //hasn't reached threshold
					if (firePressed) {
						//System.out.print(" not yet ");
					}
				}
			
		}
		
	}
	
	private void checkLaserCollision(float delta) {

		for (Laser laser : world.getLasers()) {
			Rectangle nextLaser = rectPool2.obtain();
			nextLaser = laser.getBounds();
			nextLaser.x += ((laser.getVelocity().x / 2) * delta);
			int startX = 0, startY = 0, endX = 0, endY = 0;
			if (laser.getVelocity().x < 0) {
				startX = 0;
				endX = (int) nextLaser.x;
				startY = (int) (nextLaser.y  - 1);
				endY = (int) (nextLaser.y + 1);
			} else if (laser.getVelocity().x > 0){
				endX = 10;
				startX = (int) nextLaser.x;
				startY = (int) (nextLaser.y  - 1);
				endY = (int) (nextLaser.y + 1);
			}
			populateCollidableBlocks(startX, startY, endX, endY);
			for (Enemy enemy : world.getEnemies()) {
				if (nextLaser.overlaps(enemy.getBounds()))	{
					world.getEnemies().removeIndex(world.getEnemies().indexOf(enemy, false));
					world.getLasers().removeIndex(world.getLasers().indexOf(laser, false));
				}
			}
			for (Block block : collidable) {
				if (block == null) {
					continue;
				}
				world.getCollisionRects().add(block.getBounds());
				if (nextLaser.overlaps(block.getBounds())) {
					int i = lasers.indexOf(laser, false);
					if (i != -1) {
						lasers.removeIndex(lasers.indexOf(laser, false));				
					}
					world.getCollisionRects().add(block.getBounds());
				}
			}
			
		}
		
		
	}

	private void checkCollisionWithBlocks(float delta) {
		bob.getVelocity().scl(delta);
		Rectangle bobRect = rectPool.obtain();
		bobRect.set(bob.getBounds().x, bob.getBounds().y, bob.getBounds().width, bob.getBounds().height);
		int startX, endX;
		int startY = (int)bob.getBounds().y;
		int endY = (int) (bob.getBounds().y + bob.getBounds().height);
		if (bob.getVelocity().x < 0) {
			startX = endX = (int) Math.floor(bob.getBounds().x + bob.getVelocity().x);
		} else {
			startX = endX = (int) Math.floor(bob.getBounds().x + bob.getBounds().width + bob.getVelocity().x);
		}
		populateCollidableBlocks(startX, startY, endX, endY);
		bobRect.x += bob.getVelocity().x;
		world.getCollisionRects().clear();
		for (Block block : collidable) {
			if (block == null) continue;
			if (bobRect.overlaps(block.getBounds())) {
				bob.getVelocity().x = 0;
				world.getCollisionRects().add(block.getBounds());
				break;
			}
		}
		bobRect.x = bob.getPosition().x;
		startX = (int) bob.getBounds().x;
		endX = (int) (bob.getBounds().x + bob.getBounds().width);
		if (bob.getVelocity().y < 0) {
			startY = endY = (int) Math.floor(bob.getBounds().y + bob.getVelocity().y);
		} else {
			startY = endY = (int) Math.floor(bob.getBounds().y + bob.getBounds().height + bob.getVelocity().y);
		}
		populateCollidableBlocks(startX, startY, endX, endY);
		bobRect.y += bob.getVelocity().y;
		for (Block block : collidable) {
			if (block == null) continue;
			if (bobRect.overlaps(block.getBounds())) {
				if (bob.getVelocity().y < 0) {
					grounded = true;
				}
				bob.getVelocity().y = 0;
				world.getCollisionRects().add(block.getBounds());
				break;
			}
		}
		bobRect.y = bob.getPosition().y;
		bob.getPosition().add(bob.getVelocity());
		bob.getBounds().x = bob.getPosition().x;
		bob.getBounds().y = bob.getPosition().y;
		bob.getVelocity().scl(1 / delta);
	}

	private void populateCollidableBlocks(int startX, int startY, int endX, int endY) {
		collidable.clear();
		for (int x = startX; x <= endX; x++) {
			for (int y = startY; y <= endY; y++) {
				if (x >= 0 && x < world.getLevel().getWidth() && y >=0 && y < world.getLevel().getHeight()) {
					collidable.add(world.getLevel().get(x, y));
				}
			}
		}
	}

	/** Change Bob's state and parameters based on input controls **/
	private boolean processInput() {
		
		if (keys.get(Keys.JUMP)) {
			if (!bob.getState().equals(State.JUMPING)) {
				jumpingPressed = true;
				jumpPressedTime = System.currentTimeMillis();
				bob.setState(State.JUMPING);
				bob.getVelocity().y = MAX_JUMP_SPEED;
				grounded = false;
			} else {
				if (jumpingPressed && ((System.currentTimeMillis() - jumpPressedTime) >= LONG_JUMP_PRESS)) {
					jumpingPressed = false;
				} else {
					if (jumpingPressed) {
						bob.getVelocity().y = MAX_JUMP_SPEED;
					}
				}
			}
		}
		if (keys.get(Keys.LEFT)) {
			// left is pressed
			bob.setFacingLeft(true);
			if (!bob.getState().equals(State.JUMPING)) {
				bob.setState(State.WALKING);				
			}
			bob.getAcceleration().x = -ACCELERATION;
		}
		else if (keys.get(Keys.RIGHT)) {
			// right is pressed
			bob.setFacingLeft(false);
			if (!bob.getState().equals(State.JUMPING)) {
				bob.setState(State.WALKING);				
			}
			bob.getAcceleration().x = ACCELERATION;
		}
		// need to check if both or none direction are pressed, then Bob is idle
		else {
			if (!bob.getState().equals(State.JUMPING)) {
				bob.setState(State.IDLE);
			}
			bob.getAcceleration().x = 0;
		}
		return false;
	}
}
